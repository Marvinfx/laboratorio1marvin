package com.example.lab1marvin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.lab1marvin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    //Views

    private lateinit var etEdad: EditText
    private lateinit var btnProcesar: Button
    private lateinit var tvResultado: TextView
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initView()


        btnProcesar.setOnClickListener {
            if (etEdad.text.toString().isEmpty()) {
                Toast.makeText(this, "Campo vacío", Toast.LENGTH_LONG).show()
                Log.e("$etEdad", "entro aqui")
            } else {
                val edad = etEdad.text.toString().toInt()
                when (edad) {
                    in 0..17 -> tvResultado.text = "Es menor de edad"
                    in 18..120 -> tvResultado.text = "Es mayor de edad"
                    else -> tvResultado.text = "Edad incorrecta"
                }

            }

        }

    }

    private fun initView() {
        etEdad = binding.etEdad
        btnProcesar = binding.btnProcesar
        tvResultado = binding.tvResultado
    }
}